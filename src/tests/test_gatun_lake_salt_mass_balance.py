# Testing mass balance for Gatun lake
# M. G. Castrellon | 12 March 2024

# %%

# Load libraries
import os
import sys
import pandas as pd
import matplotlib.pyplot as plt

# Load ML libraries
import torch
import torch.nn as nn

# Set the working directory
os.chdir("D:/SURFdrive/Projects/Conceptual_Model")

# Load custom modules
sys.path.append("src/model")
import utilities_and_more as utils
import mass_balance as mb
import criteria as crit

# %%

# Initialize salinity mass balance model

## Select the device for training
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

## Load the data
folder_in = "./data/processed/mass_balance"
main_df = pd.read_feather(os.path.join(folder_in, "main_df.ftr"))
main_df.set_index("Date", inplace=True)

## Define initial conditions
lake_area = 436 * 10**6 # m2
V0 = 26 * lake_area # m3
C0 = 0.05 # psu

## Define properties
properties = {'Vac': 427 * 55 * 18.3, # m3
              'Vcc': 427 * 55 * 18.3, # m3
              'Vgt': 304.8 * 33.53 * 12.5,
              'Vpm': 304.8 * 33.53 * 12.5}

## Convert the data to tensor dictionary
inputs = utils.df_to_tensor_dict(main_df, device)

## Define the model parameters
params_list = ['k', 'eta_ac', 'eta_cc', 'eta_gt', 'eta_pm',
               'omega_ac', 'omega_cc', 'omega_gt', 'omega_pm']

## Define features list
ft_list = ['Volume', 'Lock_Total_Discharges', 
           # 'Water_Quality_Discharges',
           'Transits_Panamax', 'Transits_NeoPanamax',
           'S_Atlantic_Ocean', 'S_Pacific_Ocean', 
           'S_Miraflores', 'S_Gatun_Lake']

## Initialize the model
model = mb.DifferentiableMassBalance(
    C0=C0, prop=properties, params_list=params_list,
    features_list=ft_list, device=device, dt=1,
    hidden_size=12
)


# %%

# Train the model

## Split training and testing data
train_df = main_df.loc['2000':'2019']
test_df = main_df.loc['2019':'2024']
train_data = utils.df_to_tensor_dict(train_df, device)
test_data = utils.df_to_tensor_dict(test_df, device)

## Define the loss function
loss_function = nn.MSELoss()

## Define the optimizer
optimizer = torch.optim.SGD(model.parameters(), lr=0.001)

# Extract h_observed
y_true = train_data['S_Gatun_Lake']
y_true = y_true.to(device)

# torch.autograd.set_detect_anomaly(True)

## Start training loop
model.train()
model.to(device)
nepochs = 10
print_freq = 1
train_losses = []
for epoch in range(1, nepochs+1):
    y_pred = model(train_data) # Make prediction (forward pass)
    loss = loss_function(y_true, y_pred) # Compute error/loss
    loss.backward() # Compute gradients (backpropagation)
    train_losses.append(loss.item()) # Append loss to list
    optimizer.step() # Update network parameter/weights
    optimizer.zero_grad() # Reset gradients
    if epoch % print_freq == 0:
        print(f"Epoch: {epoch}, \tLoss = {loss.item():0.4f}")

# %%
