# Testing water balance for Gatun lake
# M. G. Castrellon | 5 March 2024

# %%

# Load libraries
import os
import sys
import glob
import joblib
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Set the working directory
os.chdir("D:/SURFdrive/Projects/Conceptual_Model")

# Load custom modules
sys.path.append("src/model/lake_model")
import water_balance as wb
import utilities as utils

# %% 

# Load data

## Create list of files
folder_in = "./data/interim/hydromet"
glob_pattern = os.path.join(folder_in, "*.csv")
file_list = glob.glob(glob_pattern)

## Read files into a dataframe
df = pd.concat(
    (pd.read_csv(f) for f in file_list), 
    ignore_index=True
)

#%%

# Organize data

## Filter data by date
df['Date'] = pd.to_datetime(df['Date'])
df = df.loc[df['Date'] >= '2013-01-01', :]
df = df.loc[df['Date'] <= '2022-12-31', :]

## Filter data by label
df = df.loc[df['Label'].isin([
    'Precipitation_20Stations', 
    'Evaporation', 
    'Madden_Dam', 
    'Gatun_Dam', 
    'Flow_Total_Runoff', 
    'Water_Intakes', 
    'Lock_Total_Discharges',
    'Water_Level'
]), :]

## Pivot table
df_pivot = df.pivot_table(
    index='Date', 
    columns='Label', 
    values='Value'
)

## Rename columns
names_dict = {'Precipitation_20Stations': 'Precipitation'}
df_pivot.rename(columns=names_dict, inplace=True)

## Calculate total runoff - Madden dam releases
df_pivot['Runoff'] = df_pivot['Flow_Total_Runoff']

## Drop name of columns
df_pivot.columns.name = None

## Convert Evaporation and Precipitation to m/day
for key in ['Evaporation', 'Precipitation']:
    df_pivot[key] = df_pivot[key] / 1000

## Fill missing values with 0
df_pivot['Precipitation'] = df_pivot['Precipitation'].fillna(0)

# %%

# Run water balance model

## Transform data into array dictionary
data_dict = utils.df_to_array_dict(df_pivot)

## Define lake level-volume model
# Polynomial regression is of the form y = ax² + bx
def volume_from_level(h):
    return 8.61e+06 * h**2 + -2.38e+07 * h

## Define initial conditions
h0 = 26 # Initial water level (m)
V0 = volume_from_level(h0) # m3

## Get observed water level and convert to volume
h_obs = data_dict['Water_Level']
volume_observed = volume_from_level(h_obs)

## Define path for lake volume-area model
model_path = "./models/area_from_volume.pkl"
area_from_volume = joblib.load(model_path)

## Define and run model
wb_model = wb.LakeWaterBalance(V0=V0, area_vol_path=model_path)
volume_predicted = wb_model.simulate(data_dict)

## Plot results
plt.plot(volume_observed, label='Observed')
plt.plot(volume_predicted, label='Predicted')
plt.legend()
plt.show()

# %%


