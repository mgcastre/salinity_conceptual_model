# Script for Panama Canal's Lock Model
# M. G. Castrellon | 30 May 2024

# Required Libraries
import numpy as np
import pandas as pd
import hydrodynamics as hd
from datetime import datetime

# Define classes

class WaterSavingBasin:

    def __init__(self, length, width, H0, S0, Hf):
        self.length = length
        self.width = width
        self.area = length * width
        self.water_level = [H0 - Hf]
        self.floor_level = Hf
        self.salinity = [S0]
        self.water_volume = self.calculate_volume()
    
    def calculate_volume(self):
        self.water_volume = self.area * self.water_level
    
    def calc_simple_mass_balance(self, V_in, V_out, S_in):
        V0 = self.water_volumne[:-1]
        V1 = V0 + V_in - V_out
        S0 = self.salinity[:-1]
        S1 = (S0*(V0 - V_out) + S_in*V_in) / V1
        self.salinity.append(S1)
        self.water_volume.append(V1)

class LockChamber:
    
    def __init__(self, length, width, z_bottom, H0=None, S0=None):
        self.length = length
        self.width = width
        self.z_bottom = z_bottom
        self.area = length*width
        if H0 is not None and S0 is not None:
            self.add_initial_conditions(H0, S0)
    
    def change_length(self, new_length):
        self.length = new_length
    
    def add_initial_conditions(self, H0, S0):
        V0 = (H0 - self.z_bottom)*self.area
        self.water_volume = [V0]
        self.water_level = [H0]
        self.salinity = [S0]
        self.time = [0] # minutes
    
    def add_ship(self, V_ship):
        self.V_ship = V_ship
    
    def get_current_salinity(self):
        return self.salinity[-1]
    
    def get_current_volume(self):
        return self.water_volume[-1]
    
    def get_current_level(self):
        return self.water_level[-1]
    
    def get_current_status(self):
        V = self.get_current_volume()
        S = self.get_current_salinity()
        return V, S

    def update_status(self, V, S, H, ts):
        self.time.append(ts) # minutes
        self.water_level.append(H)
        self.water_volume.append(V)
        self.salinity.append(S)
    
    def drain_chamber(self, H_final, ts):
        V_final = (H_final - self.z_bottom)*self.area
        # Although water level changes, salinity remains constant
        self.update_status(V=V_final, S=self.salinity[-1], H=H_final, ts=ts)
    
    def ship_enters(self, V_lhs, S_lhs, ts):
        V_init, S_init = self.get_current_status()
        V_final = V_init - self.V_ship
        S_final = (S_init*(V_init - V_lhs - self.V_ship) + V_lhs*S_lhs) / V_final
        # Although volume of water gets exchanged, the water level remains constant
        self.update_status(V=V_final, S=S_final, H=self.water_level[-1], ts=ts)
    
    def fill_chamber(self, H_final, S_lift, ts):
        V_init, S_init = self.get_current_status()
        dH = H_final - self.water_level[-1]
        V_lift = dH*self.area
        V_final = V_init + V_lift
        S_final = (V_lift*S_lift + V_init*S_init) / V_final
        self.update_status(V=V_final, S=S_final, H=H_final, ts=ts)
    
    def ship_leaves(self, V_rhs, S_rhs, ts):
        V_init, S_init = self.get_current_status()
        V_final = V_init + self.V_ship
        S_final = (S_init*(V_init - V_rhs) + S_rhs*(V_rhs + self.V_ship)) / V_final
        # Although volume of water gets exchanged, the water level remains constant
        self.update_status(V=V_final, S=S_final, H=self.water_level[-1], ts=ts)


class ThreeStepLock:

    def __init__(self, lock_length, lock_width, 
                 lock_bottom_elevs, lock_head_sills):
        # Initialize lock chamber objects
        self.chambers = {}
        for cham in ['LC', 'MC', 'UC']:
            self.chambers[cham] = LockChamber(
                length=lock_length, width=lock_width,
                z_bottom=lock_bottom_elevs[cham],
                S0=None, H0=None
            )
        # Create attributes for lock heads
        self.lock_heads = {'Z': lock_head_sills}
        self.lock_heads['Chambers'] = {
            'LH1': ['UC'], 
            'LH2': ['MC', 'UC'],
            'LH3': ['LC', 'MC'], 
            'LH4': ['LC']
        }
    
    def calc_operational_levels(self, H_lake, H_ocean):
        # Extract chamber areas
        Au = self.chambers['UC'].area
        Am = self.chambers['MC'].area
        Al = self.chambers['LC'].area
        # Calculate operational levels
        denominator = ((Am + Al)*(Au + Am)/Am) - Am
        numerator = H_ocean*Al + (Am + Al)*H_lake*Au/Am
        uc_high = H_lake
        uc_low = numerator/denominator
        mc_high = uc_low
        mc_low = ((Au + Am)/Am)*mc_high - H_lake*Au/Am
        lc_high = mc_low
        lc_low = H_ocean
        # Store operational levels in a dictionary
        operational_levels = {}
        operational_levels['UC'] = (uc_low, uc_high)
        operational_levels['MC'] = (mc_low, mc_high)
        operational_levels['LC'] = (lc_low, lc_high)
        # Return operational levels
        return operational_levels
    
    def set_initial_conditions(self, boundary_conditions, salinities, 
                               direction, operation_start_dt, 
                               water_temperature=28):
        # Calculate initial operational water levels
        cham_levels = {}
        H_lake = boundary_conditions['H_lake']
        H_ocean = boundary_conditions['H_ocean']
        op_levels = self.calc_operational_levels(H_lake, H_ocean)
        if direction == 'up':
            # For uplockage, the lower chamber is at the level of the ocean
            # and the rest of the chambers are at their higher operational levels.
            cham_levels['LC'] = H_ocean
            cham_levels['MC'] = op_levels['MC'][1]
            cham_levels['UC'] = op_levels['UC'][1]
        if direction == 'down':
            # For downlockage, the upper chamber is at the level of the lake
            # and the rest of the chambers are at their lower operational levels.
            cham_levels['UC'] = H_lake
            cham_levels['MC'] = op_levels['MC'][0]
            cham_levels['LC'] = op_levels['LC'][0]
        # Add initial conditions to the lock chambers.
        for cham in ['LC', 'MC', 'UC']:
            self.chambers[cham].add_initial_conditions(
                H0=cham_levels[cham], S0=salinities[cham]
            )
        # Pass the temperature to the class attribute
        self.T = water_temperature
        # Add master initial operation start time
        self.operation_start_dt = operation_start_dt
        # Initialize dict to store salt mass load to the lake
        self.salt_mass_load = {'DC': [], 'VD': [], 'Eff': [], 
                               'V_ex': [], 'S_uc': []}
    
    def extract_properties(self, cham):
        W = self.chambers[cham].width
        L = self.chambers[cham].length
        H = self.chambers[cham].get_current_level()
        return W, L, H
    
    def lock_exchange_factor(self, lock_head, S_boundary=None):
        # Extract salinity and water levels
        if lock_head == 'LH1':
            _, L, H = self.extract_properties('UC')
            S_lhs = self.chambers['UC'].get_current_salinity()
            S_rhs = S_boundary
        elif lock_head == 'LH2':
            _, L, H = self.extract_properties('MC')
            S_lhs = self.chambers['MC'].get_current_salinity()
            S_rhs = self.chambers['UC'].get_current_salinity()
        elif lock_head == 'LH3':
            _, L, H = self.extract_properties('LC')
            S_lhs = self.chambers['LC'].get_current_salinity()
            S_rhs = self.chambers['MC'].get_current_salinity()
        elif lock_head == 'LH4':
            _, L, H = self.extract_properties('LC')
            S_rhs = self.chambers['LC'].get_current_salinity()
            S_lhs = S_boundary
        # Calculate density of water in the lock chambers
        rho_lhs = hd.Rho_from_PSU(Salt=S_lhs, Temp=self.T)
        rho_rhs = hd.Rho_from_PSU(Salt=S_rhs, Temp=self.T)
        # Sort densities
        rho1, rho2 = (rho_rhs, rho_lhs) \
            if rho_lhs > rho_rhs else (rho_lhs, rho_rhs)
        # Calculate the exchange coefficient
        tOpen = self.tGateOpen[lock_head]*60 # seconds
        head = H - self.lock_heads['Z'][lock_head]
        Eff = hd.exchange_coefficient(
            rho1=rho1, rho2=rho2, H=head, 
            L=L, tOpen=tOpen, eta=0.8)
        # Return the exchange coefficient
        return Eff
    
    def calc_equalization_level(self, cham1, cham2):
        A1 = self.chambers[cham1].area
        A2 = self.chambers[cham2].area
        H1 = self.chambers[cham1].get_current_level()
        H2 = self.chambers[cham2].get_current_level()
        Hf = (A1*H1 + A2*H2) / (A1 + A2)
        return Hf
    
    def calc_equalization_time(self, cham1, cham2, Eff):
        pass
    
    def equalize_levels(self, lower_cham, upper_cham, ts):
        # Equalize levels between chambers
        ## 1. Calculate final level for equalization
        Hf = self.calc_equalization_level(lower_cham, upper_cham)
        ## 2. Drain upper chamber to equalization level
        self.chambers[upper_cham].drain_chamber(H_final=Hf, ts=ts)
        ## 3. Fill lower chamber to equalization level
        S_next_cham = self.chambers[upper_cham].get_current_salinity()
        self.chambers[lower_cham].fill_chamber(H_final=Hf, S_lift=S_next_cham, ts=ts)
    
    def equalize_and_cross(self, lock_head, direction, init_time):
        # 1. Assign order of chambers depending on direction
        lower_cham, upper_cham = self.lock_heads['Chambers'][lock_head]
        cham1, cham2 = (lower_cham, upper_cham) \
            if direction == 'up' else (upper_cham, lower_cham)
        # 2. Equalize levels between chambers
        ts = init_time + self.eqTime[cham1] # minutes
        self.equalize_levels(lower_cham, upper_cham, ts)
        # 3. Open lock gates and move ship between chambers
        ## 2.1. Calculate volume of water to be exchanged
        Eff = self.lock_exchange_factor(lock_head)
        V_ex = self.calc_volume_exchanged(Eff=Eff, cham=upper_cham)
        ## 2.2 Calculate time stamp after crossing lock head (in minutes)
        ts = ts + self.tGateOpen[lock_head]
        ## 2.3 Move ship between chambers
        S_cham1 = self.chambers[cham1].get_current_salinity()
        S_cham2 = self.chambers[cham2].get_current_salinity()
        self.chambers[cham1].ship_leaves(V_rhs=V_ex, S_rhs=S_cham2, ts=ts)
        self.chambers[cham2].ship_enters(V_lhs=V_ex, S_lhs=S_cham1, ts=ts)
        return ts
    
    def calc_salt_mass_load(self, S_lake, V_ex_lake, S_chamber, direction):
        rho_chamber = hd.Rho_from_PSU(Salt=S_chamber, Temp=self.T)
        rho_lake = hd.Rho_from_PSU(Salt=S_lake, Temp=self.T)
        m_dc = V_ex_lake*(rho_chamber - rho_lake)
        if direction == 'up':
            m_vd = (rho_lake - rho_chamber)*self.V_ship
        elif direction == 'down':
            m_vd = (rho_chamber - rho_lake)*self.V_ship
        self.salt_mass_load['DC'].append(m_dc)
        self.salt_mass_load['VD'].append(m_vd)
    
    def calc_volume_exchanged(self, Eff, cham):
        Hf = self.chambers[cham].get_current_level()
        if cham == 'UC':
            h = Hf - self.lock_heads['Z']['LH1']
        else:
            h = Hf - self.chambers[cham].z_bottom
        A = self.chambers[cham].area
        V_ex = Eff*(A*h - self.V_ship)
        return V_ex
    
    def exchange_with_lake(self, S_lake, direction):
        # Calculate volume of water to be exchanged
        Eff = self.lock_exchange_factor(lock_head='LH1', S_boundary=S_lake)
        V_ex_lake = self.calc_volume_exchanged(Eff=Eff, cham='UC')
        # Calculate salt mass load to the lake
        S_chamber = self.chambers['UC'].get_current_salinity()
        self.calc_salt_mass_load(S_lake, V_ex_lake, S_chamber, direction)
        # Return and append values
        self.salt_mass_load['S_uc'].append(S_chamber)
        self.salt_mass_load['V_ex'].append(V_ex_lake)
        self.salt_mass_load['Eff'].append(Eff)
        return V_ex_lake
    
    def exchange_with_ocean(self, S_ocean):
        Eff = self.lock_exchange_factor(lock_head='LH4', S_boundary=S_ocean)
        V_ex_ocean = self.calc_volume_exchanged(Eff=Eff, cham='LC')
        return V_ex_ocean
    
    def calc_elapsed_minutes(self, dt_string2, time_format='%Y-%m-%d %H:%M:%S'):
        """
        Calculate the elapsed time in minutes between the start of the lock operation and the
        start of an individual lockage. Both date time stamps must be in the same time format,
        which can be specified by the user.
        """	
        dt_string1 = self.operation_start_dt
        date1 = datetime.strptime(dt_string1, time_format)
        date2 = datetime.strptime(dt_string2, time_format)
        elapsed_minutes = (date2 - date1).total_seconds()/60
        return elapsed_minutes
    
    def operate(self, operation_params, boundary_conditions):
        for i in range(len(operation_params)):
            this_transit = operation_params[i]
            self.transit(this_transit, boundary_conditions[i])
            try:
                next_transit = operation_params[i+1]
            except IndexError:
                break
            if this_transit['Direction'] != next_transit['Direction']:
                ts = self.calc_elapsed_minutes(next_transit['TS_LocksReady']) - 30
                self.turnaround(boundary_conditions[i+1], next_transit['Direction'], tinit=ts)
    
    def transit(self, operation_params, boundary_conditions):
        # Extract volume of the ship transiting the lock
        self.V_ship = operation_params['V_ship']
        # Extract boundary conditions
        S_ocean = boundary_conditions['S_ocean']
        H_ocean = boundary_conditions['H_ocean']
        S_lake = boundary_conditions['S_lake']
        H_lake = boundary_conditions['H_lake']
        # Add chamber's length and ship's volume to lock chamber
        for cham in ['LC', 'MC', 'UC']:
            L = operation_params['Chamber_Length']
            self.chambers[cham].add_ship(self.V_ship)
            self.chambers[cham].change_length(L)
        # Extract lock operation parameters
        self.tGateOpen = operation_params['tGateOpen']
        self.eqTime = operation_params['eqTime']
        # Extract lockage direction
        direction = operation_params['Direction']
        # Calculate initial lockage time stamp in minutes
        lockage_start_dt = operation_params['TS_LockageStarts']
        initial_time = self.calc_elapsed_minutes(lockage_start_dt)
        # Perform transit based on the direction
        if direction == 'up':
            self.uplockage(initial_time, S_ocean, H_ocean, S_lake, H_lake)
        elif direction == 'down':
            self.downlockage(initial_time, S_ocean, H_ocean, S_lake, H_lake)
        else:
            raise ValueError("Direction must be either 'up' or 'down'.")
    
    def uplockage(self, initial_time_stamp, S_ocean, H_ocean, S_lake, H_lake):
        ## 1) Drain the lock chamber to the level of the ocean (LH4)
        if self.chambers['LC'].get_current_level() > H_ocean:
            self.chambers['LC'].drain_chamber(H_final=H_ocean, ts=initial_time_stamp)
        ## 2) Gates at LH4 open, salinity enters from the ocean and ship enters the lock
        t_transit = self.tGateOpen['LH4'] # minutes
        time_stamp = initial_time_stamp + t_transit # minutes
        V_ex_ocean = self.exchange_with_ocean(S_ocean=S_ocean)
        self.chambers['LC'].ship_enters(V_lhs=V_ex_ocean, S_lhs=S_ocean, ts=time_stamp)
        ## 3) Equalization and transit between LC and MC
        time_stamp = self.equalize_and_cross(lock_head='LH3', direction='up', init_time=time_stamp)
        ## 4) Equalization and transit between MC and UC
        time_stamp = self.equalize_and_cross(lock_head='LH2', direction='up', init_time=time_stamp)
        ## 5) Lift the ship to the level of the lake (LH1)
        time_stamp = time_stamp + self.eqTime['UC'] # minutes to fill chamber
        self.chambers['UC'].fill_chamber(H_final=H_lake, S_lift=S_lake, ts=time_stamp)
        ## 6) Gates at LH1 open, salt mass enters the lake and ship leaves the lock
        t_transit = self.tGateOpen['LH1'] # minutes
        time_stamp = time_stamp + t_transit # minutes
        V_ex_lake = self.exchange_with_lake(S_lake=S_lake, direction='up')
        self.chambers['UC'].ship_leaves(V_rhs=V_ex_lake, S_rhs=S_lake, ts=time_stamp)

    def downlockage(self, initial_time_stamp, S_ocean, H_ocean, S_lake, H_lake):
        ## 1) Lift upper chamber to level of the lake (LH1)
        if self.chambers['UC'].get_current_level() < H_lake:
            self.chambers['UC'].fill_chamber(H_final=H_lake, S_lift=S_lake, ts=initial_time_stamp)
        ## 2) Gates at LH1 open, salt mass enters the lake and ship enters the lock
        t_transit = self.tGateOpen['LH1'] # minutes
        time_stamp = initial_time_stamp + t_transit # minutes
        V_ex_lake = self.exchange_with_lake(S_lake=S_lake, direction='down')
        self.chambers['UC'].ship_enters(V_lhs=V_ex_lake, S_lhs=S_lake, ts=time_stamp)
        ## 3) Equalization and transit between UC and MC
        time_stamp = self.equalize_and_cross(lock_head='LH2', direction='down', init_time=time_stamp)
        ## 4) Equalization and transit between MC and LC
        time_stamp = self.equalize_and_cross(lock_head='LH3', direction='down', init_time=time_stamp)
        ## 5) Drain to the level of the ocean (LH4)
        time_stamp = time_stamp + self.eqTime['LC'] # minutes to drain chamber
        self.chambers['LC'].drain_chamber(H_final=H_ocean, ts=time_stamp)
        ## 6) Gates at LH4 open, salinity enters from the ocean and ship leaves the lock
        t_transit = self.tGateOpen['LH4'] # minutes
        time_stamp = time_stamp + t_transit # minutes
        V_ex_ocean = self.exchange_with_ocean(S_ocean=S_ocean)  
        self.chambers['LC'].ship_leaves(V_rhs=V_ex_ocean, S_rhs=S_ocean, ts=time_stamp)
    
    def turnaround(self, boundary_conditions, new_direction, tinit):
        # Extract boundary conditions
        H_ocean = boundary_conditions['H_ocean']
        S_lake = boundary_conditions['S_lake']
        H_lake = boundary_conditions['H_lake']
        # Calculate lock operational levels
        op_levels = self.calc_operational_levels(H_lake, H_ocean)
        # A) From downlockage to uplockage:
        if new_direction == 'up':
            ## 1. Fill UC to the level of the Lake
            ts = tinit + 10 # minutes
            self.chambers['UC'].fill_chamber(H_final=H_lake, S_lift=S_lake, ts=ts)
            ## 2. Drain UC and fill MC to the top operating level of MC
            ts = ts + 10 # minutes
            Hf_mc = op_levels['MC'][1]
            S_uc = self.chambers['UC'].get_current_salinity()
            self.chambers['UC'].drain_chamber(H_final=Hf_mc, ts=ts)
            self.chambers['MC'].fill_chamber(H_final=Hf_mc, S_lift=S_uc, ts=ts)
            ## 3. Fill UC again to the level of the ocean
            ts = ts + 10 # minutes
            self.chambers['UC'].fill_chamber(H_final=H_lake, S_lift=S_lake, ts=ts)
            ## 4. Drain LC to the level of the ocean
            if self.chambers['LC'].get_current_level() > H_ocean:
                self.chambers['LC'].drain_chamber(H_final=H_ocean, ts=ts)
        # B) From uplockage to downlockage:
        if new_direction == 'down':
            ## 1. Drain LC to the level of the ocean
            ts = tinit + 10 # minutes
            self.chambers['LC'].drain_chamber(H_final=H_ocean, ts=ts)
            ## 2. Drain MC and fill LC to the lowest operational level of MC
            ts = ts + 10 # minutes
            Hf_mc = op_levels['MC'][0]
            S_mc = self.chambers['MC'].get_current_salinity()
            self.chambers['MC'].drain_chamber(H_final=Hf_mc, ts=ts)
            self.chambers['LC'].fill_chamber(H_final=Hf_mc, S_lift=S_mc, ts=ts)
            ## 3. Drain LC again to the level of the ocean
            ts = ts + 10 # minutes
            self.chambers['LC'].drain_chamber(H_final=H_ocean, ts=ts)
            ## 4. Fill UC to the level of the lake
            if self.chambers['UC'].get_current_level() < H_lake:
                self.chambers['UC'].fill_chamber(H_final=H_lake, S_lift=S_lake, ts=ts)
    
    def get_salt_load(self, Units='ton', Dictionary=True):
        """
        Returns the salt mass load etering the lake from the upper chamber.
        By default it returns a dictionary with the keys 'DC', 'VD', and 'Total'.
        If Dictionary is set to False, it returns the values in a tuple.
        By default, it returns the mass in tonnes, but it can also be returned in 
        kilogram by setting Units to 'kg'.
        """	
        sm_dc = np.array(self.salt_mass_load['DC'])
        sm_vd = np.array(self.salt_mass_load['VD'])
        if Units == 'ton':
            sm_dc = sm_dc/1000
            sm_vd = sm_vd/1000
        m_total = sm_dc + sm_vd
        if Dictionary:
            return {'DC': sm_dc, 'VD': sm_vd, 'Total': m_total}
        else:
            return sm_dc, sm_vd, m_total
    
    def make_results_df(self):
        """Returns a dataframe with the raw results of the lock model."""
        # Create a dataframe for each chamber and append it to a list
        list_of_results = []
        for ch in ['LC', 'MC', 'UC']:
            df = pd.DataFrame(
                {'Time': self.chambers[ch].time, 
                 'Salinity': self.chambers[ch].salinity,
                 'Water_Level': self.chambers[ch].water_level,
                 'Chamber': ch}
                )
            list_of_results.append(df)
        # Concatenate the list of dataframes and return the results
        results = pd.concat(list_of_results)
        return results

    def get_results(self, variable, dt_index):
        """
        Returns a dataframe with the results of a specific variable of 
        the lock model at different time steps sepecified by the user.
        """
        # Extract the results dataframe
        results = self.make_results_df()
        # Isolate the variable of interest
        df = results.loc[:, ['Time', variable, 'Chamber']]
        # Pivot the dataframe to have the chambers as columns
        df = df.pivot(index='Time', columns='Chamber', values=variable)
        # Add a datetime column as index
        df.reset_index(inplace=True)
        initial_time = self.operation_start_dt
        df['Time'] = pd.to_timedelta(df['Time'], unit='min')
        df['Date_Time'] = pd.to_datetime(initial_time) + df['Time']
        if dt_index:
            df.set_index('Date_Time', inplace=True)
            df.drop(columns='Time', inplace=True)
        # Return the results dataframe
        return df
    
    def get_salinities(self, dt_index=True):
        """
        Returns a dataframe of the salinity of each chamber in the lock model.
        """
        df = self.get_results(variable='Salinity', dt_index=dt_index)
        return df

    
    def get_water_levels(self, dt_index=True):
        """
        Returns a dtaframe of the water level of each chamber in the lock model.
        """
        df = self.get_results(variable='Water_Level', dt_index=dt_index)
        return df
    
    # TODO: Finalize the turnaround method.

