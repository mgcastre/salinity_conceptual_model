# Neural Networks for Hybrid Model
# M. G. Castrellon | 25/03/2024

# Import libraries
import torch
import torch.nn as nn
import torch.nn.functional as F

class TwoLayerMlp(nn.Module):

    def __init__(self, n_input, n_hidden, n_output):
        super(TwoLayerMlp, self).__init__()
        self.i2h = nn.Linear(n_input, n_hidden)
        self.h2h = nn.Linear(n_hidden, n_hidden)
        self.h2o = nn.Linear(n_hidden, n_output)

    def forward(self, x):
        x = F.relu(self.i2h(x))
        x = F.relu(self.h2h(x))
        x = F.relu(self.h2o(x))
        return x
    
class SimpleANN(nn.Module):

    def __init__(self, n_input, n_hidden, n_output):
        super(SimpleANN, self).__init__()
        self.i2h = nn.Linear(n_input, n_hidden)
        self.h2h = nn.Linear(n_hidden, n_hidden)
        self.h2o = nn.Linear(n_hidden, n_output)

    def forward(self, x):
        x = torch.relu(self.i2h(x))
        x = torch.relu(self.h2h(x))
        x = self.h2o(x)
        return x