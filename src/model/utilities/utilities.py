# Functions and utilities for mass balance
# M. G. Castrellon | 12 March 2024

# Load libraries
import torch
import numpy as np
from sklearn import preprocessing
from matplotlib import pyplot as plt

# Define utilities functions

def df_to_tensor_dict(df, device):
    """
    This function converts a dataframe into a dictionary of tensors.
    The keys in the dictionary are the column names of the dataframe.
    """
    tensor_dict = {}
    for col in df.columns:
        tensor_dict[col] = torch.tensor(
            df[col].values, 
            dtype=torch.float32, 
            requires_grad=True,
            device=device
        ).view(-1, 1)
    return tensor_dict

def df_to_array_dict(df):
    """
    This function converts a dataframe into a dictionary of numpy arrays.
    The keys in the dictionary are the column names of the dataframe.
    """
    array_dict = {}
    for col in df.columns:
        array_dict[col] = np.array(df[col].values, dtype=np.float32)
    return array_dict


def split_scale_data(df, features, target, split_date):
    
    ## Split data
    new_df = df.dropna().copy()
    df_training = new_df.loc[df['Date'] < split_date]
    df_testing = new_df.loc[df['Date'] >= split_date]

    ## Select features for training and testing
    x_train = df_training.loc[:, features]
    x_test = df_testing.loc[:, features]

    ## Fit scaler to training data
    scaler = preprocessing.StandardScaler()
    scaler.fit(x_train.values)

    ## Scale training and testing features and convert to tensor
    x_train_scaled = torch.tensor(scaler.transform(x_train.values), dtype=torch.float32)
    x_test_scaled = torch.tensor(scaler.transform(x_test.values), dtype=torch.float32)

    ## Select targets for training and testing
    y_test = df_testing.loc[:, target]
    y_train = df_training.loc[:, target]

    ## Convert targets to tensor
    y_train = torch.tensor(y_train.values, dtype=torch.float32)
    y_test = torch.tensor(y_test.values, dtype=torch.float32)

    ## Reshape target tensors
    y_train = y_train.view(-1, 1)
    y_test = y_test.view(-1, 1)

    ## Return scaled features and targets
    return x_train_scaled, x_test_scaled, y_train, y_test, scaler


def train_model(model, x_train, y_train,
                optimizer, loss_function, 
                nepochs=100, print_loss=True,
                print_freq=10):

    # Set model to training mode
    model.train()

    # Sent inputs and targets to device
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    x_train = x_train.to(device)
    y_train = y_train.to(device)
    model = model.to(device)

    ## Start training loop
    train_losses = []
    print("Training model...\n")
    for epoch in range(1, nepochs+1):
        y_pred = model(x_train) # Make prediction (forward pass)
        loss = loss_function(y_train, y_pred) # Compute error/loss
        loss.backward() # Compute gradients (backpropagation)
        train_losses.append(loss.item()) # Append loss to list
        optimizer.step() # Update network parameter/weights
        optimizer.zero_grad() # Reset gradients
        if print_loss and epoch % print_freq == 0:
            print(f"Epoch: {epoch}, \tLoss = {loss.item():0.4f}")
    
    # Return list with train losses
    return train_losses

def evaluate_model(model, x_test, y_test, loss_function, print_loss=True):
    
    # Set model to evaluation mode
    model.eval()

    # Sent inputs and targets to device
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    x_test = x_test.to(device)
    y_test = y_test.to(device)
    model = model.to(device)

   # Evaluate the model
    with torch.no_grad():
        y_pred = model(x_test)
        eval_loss = loss_function(y_test, y_pred)
    if print_loss:
        print("\nEvaluation/Test Loss =", np.round(eval_loss.item(), 4))

    # Return the evaluation loss
    return eval_loss

def train_and_evaluate_model(model, x_train, x_test, y_train, y_test, 
                             optimizer, loss_function, nepochs=100, 
                             print_loss=True, print_freq=10):
    
    # Sent inputs and targets to device
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    x_train = x_train.to(device)
    y_train = y_train.to(device)
    model = model.to(device)

    ## Start training loop
    train_losses = []
    eval_losses = []
    print("Training model...\n")
    
    for epoch in range(1, nepochs+1):
        # Train the model
        model.train()
        y_pred = model(x_train)
        train_loss = loss_function(y_train, y_pred)
        train_loss.backward()
        train_losses.append(train_loss.item())
        optimizer.step()
        optimizer.zero_grad()
        
        # Evaluate the model
        with torch.no_grad():
            model.eval() 
            y_pred_eval = model(x_test)
            eval_loss = loss_function(y_test, y_pred_eval)
            eval_losses.append(eval_loss.item())
        
        # Print train and eval loss
        if print_loss and epoch % print_freq == 0:
            print(f"Epoch: {epoch}, \tTrain Loss = {train_loss.item():0.4f} \tEval Loss = {eval_loss.item():0.4f}")
    
    # Return list with train losses
    return train_losses, eval_losses

def plot_obs_sim(y_obs, y_sim, title, ylim):
    plt.figure(figsize=(12, 6))
    plt.plot(y_obs, label='Observed', linestyle='-', alpha=0.8)
    plt.plot(y_sim, label='Simulated', linestyle='--', alpha=0.8)
    plt.ylabel("Salinity (PSU)")
    plt.xlabel("Days")
    plt.title(title)
    plt.ylim([0, 9])
    plt.legend()
    plt.show()