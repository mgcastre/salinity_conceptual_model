# Criteria (Loss Functions) for Mass Balance Model
# M. G. Castrellon | 5 March 2024

# Import libraries
import torch
import torch.nn as nn

# Define criteria/error/loss functions

class RmseLoss(torch.nn.Module):
    def __init__(self):
        super(RmseLoss, self).__init__()

    def forward(self, targets, outputs):
        loss = torch.mean((outputs - targets)**2)
        loss = torch.sqrt(loss)
        return loss
    
class NrmseLoss(torch.nn.Module):
    def __init__(self):
        super(NrmseLoss, self).__init__()

    def forward(self, targets, outputs):
        loss = torch.mean((outputs - targets)**2)
        loss = torch.sqrt(loss)/torch.mean(targets)
        return loss