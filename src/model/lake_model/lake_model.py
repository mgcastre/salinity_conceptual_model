# Class for Gatun Lake's Model
# M. G. Castrellon | 30/05/2024

# Required libraries
import numpy as np
import hydrodynamics as hd

# Define classes for the lake model

class SimpleLakeModel:
    """
    Class for a simple mass balance model of a lake.

    Inputs for class initialization:
    V0: Lake's initial volume
    C0: Lake's initial salt concentration
    dt: Time step (default = 1 day)
    """
    
    # Initialize the class
    def __init__(self, V0, C0, dt=1):
        self.dt = dt # Time step (days)
        self.V0 = V0 # Initial volume (m3)
        self.C0 = C0 # Initial concentration (kg/m3)

    # Calculate salinity mass balance
    def salt_mass_balance(self, df, k, method="Euler"):
        """
        Inputs to calculate salinity mass balance methods:
        k: Decay/deposition rate of salt in the bottom of the lake (1/day)
        df: A data frame containing time series of salt mass fluxes, 
        water inflows and outflows. It requires the following columns:
            'Salt_Flux_Total' - Total salt mass flux entering the lake (kg/day)
            'Outflows' - Total water outflows from the lake, excluding evaporation (m3/day)
            'Volume' - Volume of the lake (m3)
        method: Numerical method to solve the mass balance equation. The user can choose between
        'Euler' (default), 'Runge-Kutta' or 'Implicit'.
        """	
        
        # Obtain outflows, inflows and volume
        V = df['Volume'].to_numpy()
        Qout = df['Outflows'].to_numpy()
        QinCin = df['Salt_Flux_Total'].to_numpy()
            
        # Calculate mass balance
        t = len(df)
        C = np.zeros(t)
        C[0] = self.C0
        dt = self.dt

        if method == "Euler": ## Also called explicit or forward scheme
            for i in range(0, t-1):
                C[i+1] = C[i] + ((QinCin[i] - (Qout[i] + k*V[i]) * C[i])/V[i]) * dt
        
        elif method == "Runge-Kutta":
            for i in range(0, t-1):
                TotalOut = Qout[i] + k*V[i]
                f1 = (QinCin[i] - TotalOut*C[i])/V[i]
                c1 = C[i] + (f1/2)*dt
                f2 = (QinCin[i] - TotalOut*c1)/V[i]
                c2 = C[i] + (f2/2)*dt
                f3 = (QinCin[i] - TotalOut*c2)/V[i]
                c3 = C[i] + f3*dt
                f4 = (QinCin[i] - TotalOut*c3)/V[i]
                C[i+1] = C[i] + (dt/6)*(f1 + 2*f2 + 2*f3 + f4)
        	
        elif method == "Implicit":
            # Work in progress
            message = "Please select Euler or Runge-Kutta"
            raise NotImplementedError(message)
            """
            for i in range(0, t-1):
                C[i+1] = C[i] + ((QinCin[i] - (Qout[i] + k*V[i]) * C[i+1])/V[i]) * dt
            """
        
        else:
            message = "Please select a valid method: \
                        Euler, Runge-Kutta or Implicit"
            raise ValueError(message)
        
        # Return concentration array
        return C



class GatunLakeModel:
    """
    Class for the water balance and salinity mass balance 
    models of Gatun Lake.

    Inputs for class initialization:
    
    prop: Dictionary for lake/lock properties. It requires 
    the following keys:
        'A' - Area of the lake (m2)
        'Vac' - Volume of the Agua Clara locks chamber (m3)
        'Vcc' - Volume of the Cocoli locks chamber (m3)
        'Vgt' - Volume of the Gatun locks chamber (m3)
        'Vpm' - Volume of the Pedro Miguel locks chamber (m3)
    
    params: Dictionary with static model parameters. It requires 
    the following keys:
        'eta_gt' - Exchange fraction of the Gatun locks
        'eta_pm' - Exchange fraction of the Pedro Miguel locks
        'omega_gt' - Fraction of ocean salinity for the Gatun locks
        'omega_pm' - Fraction of ocean salinity for the Pedro Miguel locks
        'k' - Decay/deposition rate of salt in the bottom of the lake
    
    V0: Lake's initial volume
    C0: Lake's initial salt concentration
    dt: Time step (default = 1 day)

    Inputs for water balance and mass balance methods:

    df: A data frame containing time series of inflows, outflows and
    input salinity. It requires the following columns:
        'Precipitation' - Precipitation over the lake (mm/day)
        'Madden_Dam' - Releases from Madden Dam (m3/day)
        'Tributaries' - Inflows from main tributaties (m3/day)
        'Evaportation' - Evaporation rate over the lake (m3/day)
        'Gatun_Dam' - Releases from Gatun Dam (m3/day)
        'Water_Intakes' - Outflows for drinking water (m3/day)
        'Lock_Total_Discharges' - Releases from lock operations (m3/day)
    """
    
    # Initialize the class
    def __init__(self, prop, params, V0, C0, dt=1):
        self.dt = dt # Time step
        self.V0 = V0 # Initial volume
        self.C0 = C0 # Initial concentration
        
    # Obtain inflows
    def get_inflows(self, df):
        A = self.prop['A'] # lake area (m2)
        Qp = df['Precipitation'].to_numpy()*A
        Qmd = df['Madden_Dam'].to_numpy()
        Qtr = df['Tributaries'].to_numpy()
        return Qp, Qmd, Qtr
    
    # Obtain outflows
    def get_outflows(self, df):
        A = self.prop['A'] # lake area (m2)
        Qe = df['Evaportation'].to_numpy()*A
        Qgd = df['Gatun_Dam'].to_numpy()
        Qwi = df['Water_Intakes'].to_numpy()
        Qld = df['Lock_Total_Discharges'].to_numpy()
        return Qe, Qgd, Qwi, Qld
    
    # Obtain number of lockages per day
    def get_transits(self, df):
        Nneo = df['Transits_NeoPanamax'].to_numpy()
        Npax = df['Transits_Panamax'].to_numpy()
        return Nneo, Npax
    
    # Calculate effective lock volumnes
    def calc_eff_lock_volumes(self):
        LVac = self.params['eta_ac'] * self.prop['Vac']
        LVcc = self.params['eta_cc'] * self.prop['Vcc']
        LVgt = self.params['eta_gt'] * self.prop['Vgt']
        LVpm = self.params['eta_pm'] * self.prop['Vpm']
        return LVac, LVcc, LVgt, LVpm
    
    # Calculate inflow from locks due to density currents
    def clac_flow_locks(self, df):
        Nneo, Npax = self.get_transits(df)
        LVac, LVcc, LVgt, LVpm = self.calc_eff_lock_volumes()
        Qlo = np.multiply(x1=Nneo, x2=(LVac + LVcc)) \
              + np.multiply(x1=Npax, x2=(LVgt + LVpm))
        return Qlo
    
    # Calculate salinity in lock chambers
    def calc_lock_salinity(self, df):
        Cac = self.params['omega_ac'] * df['S_Atlantic_Ocean'].to_numpy()
        Cgt = self.params['omega_gt'] * df['S_Atlantic_Ocean'].to_numpy()
        Ccc = self.params['omega_cc'] * df['S_Pacific_Ocean'].to_numpy()
        Cpm = self.params['omega_pm'] * df['S_Miraflores'].to_numpy()
        return Cac, Ccc, Cgt, Cpm

    # Calculate salt mass flux in
    def calc_salt_flux_in(self, df):
        Nneo, Npax = self.get_transits(df)
        Cac, Ccc, Cgt, Cpm = self.calc_lock_salinity(df)
        LVac, LVcc, LVgt, LVpm = self.calc_eff_lock_volumes()
        flux_neo = np.multiply(x1=Nneo, x2=(LVac*Cac + LVcc*Ccc))
        flux_pax = np.multiply(x1=Npax, x2=(LVgt*Cgt + LVpm*Cpm))
        flux_total = flux_neo + flux_pax
        return flux_total

    # Calculate water balance
    def water_balance(self, df):
        """
        Water Balance Model for Gatun Lake
        Qp: Precipitation volumetric flow [m3]
        Qe: Evaporation volumetric flow [m3]
        Qmd: Madden Dam discharges [m3]
        Qgd: Gatun Dam discharges [m3]
        Qtr: Flow from main tributaties [m3]
        Qlo: Inflow from lock operations [m3]
        Qwi: Outflow for drinking water [m3]
        Qld: Discharges from lock operations [m3]
        """
        
        # Obtain inflows and outflows
        Qp, Qmd, Qtr = self.get_inflows(df)
        Qe, Qgd, Qwi, Qld = self.get_outflows(df)
        Qlo = self.calc_flow_locks(df)
        
        # Calculate water balance
        t = len(df)
        V = np.zeros(t)
        V[0] = self.V0
        for i in range(0, t-1):
            V[i+1] = V[i] \
                (+ Qp[i] + Qmd[i] + Qtr[i] + Qlo[i] \
                 - Qe[i] - Qgd[i] - Qwi[i] - Qld[i] \
                ) * self.dt
        
        # Return volume vector/tensor
        return V

    # Calculate salinity mass balance
    def salt_mass_balance(self, df, method="Euler"):
        
        # Calculate flow out
        _, Qgd, Qwi, Qld = self.get_outflows(df)
        Qout = Qgd + Qwi + Qld

        # Calculate salinity inputs
        QinCin = self.salt_flux_in(df)
        
        # Calculate water balance
        V = self.water_balance(df)
        
        # Calculate mass balance
        t = len(df)
        C = np.zeros(t)
        C[0] = self.C0
        k = self.params['k']
        dt = self.dt

        if method == "Euler":
            # Also called explicit or forward scheme
            for i in range(0, t-1):
                C[i+1] = C[i] + ((QinCin[i] - Qout[i]*C[i])/V[i] - k*C[i])*dt
        
        elif method == "Runge-Kutta":
            for i in range(0, t-1):
                f1 = (QinCin[i] - Qout[i]*C[i])/V[i] - k*C[i]
                c1 = C[i] + (f1/2)*dt
                f2 = (QinCin[i] - Qout[i]*c1)/V[i] - k*c1
                c2 = C[i] + (f2/2)*dt
                f3 = (QinCin[i] - Qout[i]*c2)/V[i] - k*c2
                c3 = C[i] + f3*dt
                f4 = (QinCin[i] - Qout[i]*c3)/V[i] - k*c3
                C[i+1] = C[i] + (dt/6)*(f1 + 2*f2 + 2*f3 + f4)
        	
        elif method == "Implicit":
            # Work in progress
            message = "Please select Euler or Runge-Kutta"
            raise NotImplementedError(message)
            """
            for i in range(0, t-1):
                C[i+1] = C[i] + (QinCin[i] - (Qout[i] + k)*C[i+1])*dt/V[i]
            """
        
        else:
            message = "Please select a valid method: \
                        Euler, Runge-Kutta or Implicit"
            raise ValueError(message)
        
        # Return volume and concentration vectors/tensors
        return V, C