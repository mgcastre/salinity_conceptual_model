# Waters Balance Model for Gatun Lake
# M. G. Castrellon | 23/02/2024

# Import libraries
import joblib
import numpy as np

# Define class for the water balance
class LakeWaterBalance():

    def __init__(self, V0, area_vol_path, dt=1):
        super(LakeWaterBalance, self).__init__()
        self.load_area_from_volume_model(model_path=area_vol_path)
        self.V0 = V0
        self.dt = dt
    
    def load_area_from_volume_model(self, model_path):
        self.area_from_volume = joblib.load(model_path)
    
    def get_inflows(self):
        Hp = self.data['Precipitation']
        Qmd = self.data['Madden_Dam']
        Qro = self.data['Runoff']
        return Hp, Qmd, Qro
    
    def get_outflows(self):
        He = self.data['Evaporation']
        Qgd = self.data['Gatun_Dam']
        Qwi = self.data['Water_Intakes']
        Qld = self.data['Lock_Total_Discharges']
        return He, Qgd, Qwi, Qld

    def calc_water_balance(self, data_dict):
        self.data = data_dict
        Hp, Qmd, Qro = self.get_inflows()
        He, Qgd, Qwi, Qld = self.get_outflows()
        t = len(Hp)
        V = np.zeros(t)
        Qin = np.zeros(t)
        Qout = np.zeros(t)
        V[0] = self.V0
        dt = self.dt
        for i in range(0, t-1):
            A = self.area_from_volume.predict(V[i].reshape(-1, 1))
            Qin[i] = Hp[i]*A + Qmd[i] + Qro[i]
            Qout[i] = He[i]*A + Qgd[i] + Qwi[i] + Qld[i]
            V[i+1] = V[i] + (Qin[i] - Qout[i])*dt
        return V
    
    def simulate(self, data_dict):
        v = self.calc_water_balance(data_dict)
        return v
