# Salinity Mass Balance Model for Gatun Lake
# M. G. Castrellon | 12/12/2023

# Import libraries
import torch
import torch.nn as nn
import torch.nn.functional as F
from ann import *

# Define classes for salinity mass balance

class SalinityMassBalance(nn.Module):
    """
    Salinity mass balance model for Gatun Lake.
    """
    def __init__(self, C0, prop, device, dt=1):
        super(SalinityMassBalance, self).__init__()
        self.dt = dt
        self.C0 = C0
        self.prop = prop
        self.device = device
    
    def get_outflows(self):
        Qgd = self.data['Gatun_Dam']
        Qwi = self.data['Water_Intakes']
        Qld = self.data['Lock_Total_Discharges']
        return Qgd + Qwi + Qld
    
    def get_transits(self):
        Nneo = self.data['Transits_NeoPanamax']
        Npax = self.data['Transits_Panamax']
        return Nneo, Npax

    def get_lock_salinity(self):
        ## For Neo-Panamax Locks
        if self.params['omega_ac'] is None:
            Cac = self.data['S_Agua_Clara']
        else:
            Cao = self.data['S_Atlantic_Ocean']
            Cac = self.params['omega_ac'] * Cao
        if self.params['omega_cc'] is None:
            Ccc = self.data['S_Cocoli']
        else:
            Cpo = self.data['S_Pacific_Ocean']
            Ccc = self.params['omega_cc'] * Cpo
        ## For Panamax Locks
        Cml = self.data['S_Miraflores']
        Cao = self.data['S_Atlantic_Ocean']
        Cpm = self.params['omega_pm'] * Cml
        Cgt = self.params['omega_gt'] * Cao
        ## Return All Values
        return Cac, Ccc, Cgt, Cpm
    
    def get_eff_lock_volumes(self):
        LVac = self.params['eta_ac'] * self.prop['Vac']
        LVcc = self.params['eta_cc'] * self.prop['Vcc']
        LVgt = self.params['eta_gt'] * self.prop['Vgt']
        LVpm = self.params['eta_pm'] * self.prop['Vpm']
        return LVac, LVcc, LVgt, LVpm

    def get_salt_flux_in(self):
        Nneo, Npax = self.get_transits()
        Cac, Ccc, Cgt, Cpm = self.get_lock_salinity()
        LVac, LVcc, LVgt, LVpm = self.get_eff_lock_volumes()
        flux_neo = Nneo * (LVac*Cac + LVcc*Ccc)
        flux_pax = Npax * (LVgt*Cgt + LVpm*Cpm)
        return flux_neo + flux_pax
    
    def calc_mass_balance(self, method):
        Qout = self.get_outflows()
        QinCin = self.get_salt_flux_in()
        V = self.data['Volume']
        C = torch.zeros_like(V)
        C[0] = self.C0
        k = self.params['k']
        dt = self.dt
        t = len(V)
        if method == "Euler":
            for i in range(0, t-1):
                C[i+1] = C[i] + ((QinCin[i] - Qout[i]*C[i])/V[i] - k*C[i])*dt
        elif method == "Runge-Kutta":
            for i in range(0, t-1):
                f1 = (QinCin[i] - Qout[i]*C[i])/V[i] - k*C[i]
                c1 = C[i] + (f1/2)*dt
                f2 = (QinCin[i] - Qout[i]*c1)/V[i] - k*c1
                c2 = C[i] + (f2/2)*dt
                f3 = (QinCin[i] - Qout[i]*c2)/V[i] - k*c2
                c3 = C[i] + f3*dt
                f4 = (QinCin[i] - Qout[i]*c3)/V[i] - k*c3
                C[i+1] = C[i] + (dt/6)*(f1 + 2*f2 + 2*f3 + f4)
        else:
            message = "Please select a valid option, Euler or Runge-Kutta."
            raise ValueError(message)
        C_New = C.clone().detach().requires_grad_(True)
        return C_New
    
    def forward(self, data_dict, params_dict, method="Euler"):
        self.data = data_dict
        self.params = params_dict
        C = self.calc_mass_balance(method)
        return C
    

class DifferentiableMassBalance(nn.Module):

    def __init__(self, C0, prop, params_list,
                 features_list, device, dt=1,
                 hidden_size=15):
        super(DifferentiableMassBalance, self).__init__()
        # Initialize mass balance model
        self.model = SalinityMassBalance(C0, prop, device, dt)
        # Assign other self values
        self.params_list = params_list
        self.features_list = features_list
        self.hidden_size = hidden_size
        self.device = device
        # Initialize parameter mapping
        self.mlp = TwoLayerMlp(
            n_input=len(features_list),
            n_hidden=hidden_size,
            n_output=len(params_list)
        )
    
    def format_inputs(self, inputs):
        # Transofrm dictionary of tensors into a single 
        # tensor of shaple n_samples * n_features
        ft = self.features_list[0]
        n_samples = inputs[ft].size()[0]
        n_features = len(self.features_list)
        x = torch.zeros(n_samples, n_features)
        for j, xf in enumerate(self.features_list):
            x[:, j] = inputs[xf][:, 0]
        x = x.to(self.device)
        return x
    
    def format_parameters(self, y):
        params_dict = {}
        y = torch.mean(y, dim=0, keepdim=True)
        for j, param in enumerate(self.params_list):
            params_dict[param] = y[0, j]
        return params_dict
    
    def get_mb_parameters(self):
        return self.mb_parameters
    
    def forward(self, inputs_dict):
        x = self.format_inputs(inputs_dict)
        y = self.mlp(x) # Parameter learning
        params_dict = self.format_parameters(y)
        C = self.model(inputs_dict, params_dict)
        self.mb_parameters = params_dict
        return C

