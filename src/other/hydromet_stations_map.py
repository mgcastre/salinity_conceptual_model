# Interactive map of hydromet stations in the Panama Canal Watershed
# M. G. Castrellon | July 24th, 2024

#%%

# Load libraries
import os
import folium
import pandas as pd

# Set the working directory
os.chdir("D:/SURFdrive/Projects/Conceptual_Model")

#%%

# Load data
path = os.path.join(os.getcwd(), './data/raw/smithsonian/hydromet')
path_to_data = os.path.join(path, 'acp_weather_stations.csv')
weather_stations = pd.read_csv(path_to_data)

# Select columns of interest
weather_stations = weather_stations[['ID', 'NAME', 'TIPO', 'STATUS', 'LAT', 'LON']]
weather_stations.columns = ['ID', 'Name', 'Type', 'Status', 'Lat', 'Long']


# Filter only the active stations
active_stations = weather_stations.copy(deep=True) \
    .loc[weather_stations['Status'] == 'ACTIVE']


#%%


# Define function to build map with folium
def buildMap(df, iconColorsDict, iconColorsColumn, idColumn='ID', nameColumn='Name',
             icon='circle', zoomStart=10, controlScale=True):
    initialLocation = [df['Lat'].mean(), df['Long'].mean()]
    myMap = folium.Map(location=initialLocation, zoom_start=zoomStart, control_scale=controlScale)
    for index, row in df.iterrows():
        iidd = row[idColumn]
        name = row[nameColumn]
        iccm = row[iconColorsColumn]
        text = f'<b>ID:</b> {iidd} <br><b>Name:</b> {name} <br><b>{iconColorsColumn}:</b> {iccm}'
        label = folium.Html(text, script=True)
        folium.Marker(
            location=[row['Lat'], row['Long']], 
            popup=folium.Popup(label, max_width=2650),
            icon=folium.Icon(color=iconColorsDict[iccm], icon=icon)
            ).add_to(myMap)
    return myMap

# Create the map
iconColors = {'Active': 'green', 'Inactive': 'red', 'No Info': 'gray'}
rainGagesMap = buildMap(df=precipitationLocations, iconColorsDict=iconColors, 
                        iconColorsColumn='Status', icon='glyphicon-tint')

# Save the map
rainGagesMap.save('../results/maps/Rain Gages Locations.html')

# Visualize the map
rainGagesMap
# Script to create a map of the hydromet stations in the Panama Canal Watershed
# M. G. Castrellon | January 23rd, 2024



# Load data
pathRel = '../data/raw/hydromet/locations'
pathFull = os.path.join(os.getcwd(), pathRel)
pathData = os.path.join(pathFull, 'ACP Rainfall Stations Location.xls')
precipitationLocations = pd.read_excel(pathData, sheet_name='Manually Modified')

# Select columns of interest
myCols = ['ID', 'NAME', 'TIPO', 'STATUS', 'LAT', 'LON']
precipitationLocations = precipitationLocations[myCols].rename(columns=colRenameDict)

# Create the map
iconColors = {'Active': 'green', 'Inactive': 'red', 'No Info': 'gray'}
rainGagesMap = buildMap(df=precipitationLocations, iconColorsDict=iconColors, 
                        iconColorsColumn='Status', icon='glyphicon-tint')

# Save the map
# rainGagesMap.save('../outputs/maps/Met Locations.html')

#| label: fig-salinity-map
#| fig-cap: "Map of salinity measurement locations in the Panama Canal. The map shows the location of the 17 telemetry stations (purple) and the 193 salinity profiles (blue)."

# Create the map

## Define icon properties per category
markerOptions = {'Telemetry': {'icon':'satellite-dish', 'color':'purple'},
                 'Profile': {'icon':'ruler-vertical', 'color':'blue'},
                 'Monthly': {'icon':'calendar-alt', 'color':'orange'}}

## Initialize map
initialLocation = [salinityLocations['Lat'].mean(), salinityLocations['Long'].mean()]
lakeSalMap = folium.Map(location=initialLocation, zoom_start=10.5, control_scale=True)

## Add different tiles (base maps)
folium.TileLayer('openstreetmap').add_to(lakeSalMap)
folium.TileLayer('stamenwatercolor').add_to(lakeSalMap)
folium.TileLayer('stamenterrain').add_to(lakeSalMap)

## Start feature groups for toggle functionality
layerTelem = folium.FeatureGroup(name='Telemetry', show=True)
layerProfile = folium.FeatureGroup(name='Profile', show=False)
layerMonthly = folium.FeatureGroup(name='Monthly', show=True)
    
### Loop through the different points
for index, row in salinityLocations.iterrows():
    ### Select information for the popup
    iidd = row['Station_ID']
    name = row['Station_Name']
    telem = row['TelemetryData']
    prof = row['ProfileData']
    sensor = row['Sensor_ID']
    featureType = row['Type']
    ### Create the popup label
    text = f'<b>Station ID:</b> {iidd}  \
                <br><b>Sensor ID:</b> {sensor} \
                <br><b>Station Name:</b> {name} \
                <br><b>Station Type:</b> {featureType} \
                <br><b>Telemetry Data:</b> {telem} \
                <br><b>Profile Data:</b> {prof}'
    label = folium.Html(text, script=True)
    ### Create tooltip text
    if telem and prof:
        tooltipText = f'{sensor} | {iidd}'
    elif telem:
        tooltipText = f'{sensor}'
    else:
        tooltipText = f'{iidd}'
    ### Create the marker
    marker = folium.Marker(
        location=[row['Lat'], row['Long']], 
        icon=folium.Icon(color=markerOptions[featureType]['color'], 
                            icon=markerOptions[featureType]['icon'], prefix='fa'),
        popup=folium.Popup(label, max_width=2650),
        tooltip=tooltipText)
    ### Add the marker to the correct feature layer
    if featureType == 'Telemetry':
        marker.add_to(layerTelem)
    elif featureType == 'Profile':
        marker.add_to(layerProfile)
    elif featureType == 'Monthly':
        marker.add_to(layerMonthly)

## Add all feature layers to the map
layerTelem.add_to(lakeSalMap)
layerProfile.add_to(lakeSalMap)
layerMonthly.add_to(lakeSalMap)

## Add layer control
folium.LayerControl().add_to(lakeSalMap)

# Save the map
lakeSalMap.save('../results/maps/Salinity Measurement Locations.html')

# Visualize the map
lakeSalMap