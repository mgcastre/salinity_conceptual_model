# Download data from online repositories
# M. G. Castrellon | 8 March 2024

# Load libraries
import os
import copernicusmarine as cpm

# Define working directory
os.chdir("D:/SURFdrive/Projects/Conceptual_Model")
output_folder = os.path.join(os.getcwd(), "./data/raw")

# Download data from Copernicus Marine

## Define coordinates
coords = {'P1': (9.528, -80.180), 'P2': (9.670, -79.780),
          'P3': (8.687, -79.627), 'P4': (8.886, -79.296)}


## Subset data
datasets = {
    "Multi-Physics": 
        {"Name": "cmems_obs-mob_glo_phy-sss_my_multi_P1D", 
         "Time": ("2000-01-01T00:00:00", "2020-12-31T00:00:00")},
    "Near-Real-Time": 
        {"Name": "cmems_obs-mob_glo_phy-sss_nrt_multi_P1D",
         "Time": ("2021-01-01T00:00:00", "2023-12-31T00:00:00")}
}

for key, value in datasets.items():
    cpm.subset(
        dataset_id=value["Name"],
        variables=["sos", "sos_error"],
        minimum_longitude=coords['P1'][1],
        maximum_longitude=coords['P4'][1],
        minimum_latitude=coords['P3'][0],
        maximum_latitude=coords['P2'][0],
        start_datetime=value["Time"][0],
        end_datetime=value["Time"][1],
        minimum_depth=0,
        maximum_depth=50,
        output_filename = "CMEMS_Salinity_Panama_Canal_"+key+".nc",
        output_directory = os.path.join(output_folder, "copernicus")
    )