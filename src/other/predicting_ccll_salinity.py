# Predicting upper chamber salinity
# M. G. Castrellon | 27 March 2024

#%% 

# Load Libraries
import sys
import time
import torch
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# Set the working directory
os.chdir("D:/SURFdrive/Projects/Conceptual_Model")

# Load custom modules
sys.path.append("./src/model")
import utilities as utils
import ann as ann

#%%

# Load and preprocess data

## Load data
df_ccll = pd.read_csv("./data/interim/feature_eng/ccll_salinity.csv")

## Define features and target
target = 'S_CCLL'
features = ['S_PO', 'VT_Releases', 'WQ_Releases', 
            'Water_Level', 'Transits_NeoPanamax']

## Split and scale data
x_train, x_test, y_train, y_test, scaler = \
    utils.split_scale_data(df_ccll, features, target, '2021-01-01')

#%%

# Train artificial neural network

## Initialize model
torch.manual_seed(19)
n_features = x_train.shape[1]
model = ann.SimpleANN(n_input=n_features, n_hidden=8, n_output=1)
optimizer = torch.optim.Adam(model.parameters(), lr=0.1)
loss_function = torch.nn.MSELoss()

## Train model
nepochs = 4
train_losses = \
    utils.train_model(model, x_train, y_train,
                      optimizer, loss_function,
                      nepochs, print_freq=1) 

## Evaluate model
eval_loss = \
    utils.evaluate_model(model, x_test, y_test, 
                         loss_function)
# %%

# Plot training and testing data

## Plot observed vs. simulated (train data)
y_pred = model(x_train)
y_obs = y_train.detach().numpy()
y_sim = y_pred.detach().numpy()
title = "Observed and Simulated Salinity in Cocolí Locks (Train Data)"
utils.plot_obs_sim(y_obs, y_sim, title, ylim=[0, 10])

## Plot observed and simulated (test data)
y_pred = model(x_test)
y_obs = y_test.detach().numpy()
y_sim = y_pred.detach().numpy()
title = "Observed and Simulated Salinity in Cocolí Locks (Train Data)"
utils.plot_obs_sim(y_obs, y_sim, title, ylim=[0, 10])

# %%

# Save model
# timestr = time.strftime("%Y%m%d-%H%M%S")
# file_path = "./models/ccll_ann_" + timestr + ".pth"
# torch.save(model.state_dict(), file_path)

# %%

# Predict salinity

## Select features
df_ccll['Date'] = pd.to_datetime(df_ccll['Date'])
df_ccll = df_ccll.loc[df_ccll['Date'] >= '2016-06-01'].copy()
df_x = df_ccll.loc[:, features]

## Scale features
x = scaler.transform(df_x.values)
x = torch.tensor(x, dtype=torch.float32)

## Make prediction
model.eval()
y = model(x).detach().numpy()

## Plot results
plt.figure(figsize=(10, 6))
plt.plot(df_ccll['Date'], df_ccll['S_CCLL'], label='Observed', alpha=0.8)
plt.plot(df_ccll['Date'], y, label='Simulated', linestyle='--', alpha=0.7)
plt.title("Observed and Simulated Salinity in Cocolí Locks")
plt.ylabel("Salinity (PSU)")
plt.show()

# %%

# Save predicted salinity
df_ccll['S_CCLL_Pred'] = y
file_name = "ccll_salinity_pred_ann0.csv"
df_ccll.rename(columns={'S_CCLL': 'S_CCLL_Obs'}) \
    .loc[:, ['Date', 'S_CCLL_Obs', 'S_CCLL_Pred']] \
    .to_csv("./data/interim/predictions/"+file_name, index=False)

# %%
