# Prepare data for mass balance model
# M. G. Castrellon | 19 March 2024

# Load libraries
import os
import sys
import glob
import pandas as pd


# Set the working directory
os.chdir("D:/SURFdrive/Projects/Conceptual_Model")

# %% 

# Load and prepare hydromet data

## Create list of files
folder_in = "./data/interim/hydromet"
glob_pattern = os.path.join(folder_in, "*.csv")
file_list = glob.glob(glob_pattern)

## Read files into a dataframe and pivot
hydromet_data = pd.concat(
    (pd.read_csv(f) for f in file_list), 
    ignore_index=True
    ).pivot_table(
        index='Date', 
        columns='Label', 
        values='Value'
    )

## Drop name of columns
hydromet_data.columns.name = None

## Select columns of interest from hydromet data
my_cols = ["Lock_Total_Discharges", "Gatun_Dam", "Water_Intakes"]
hydromet_data = hydromet_data.loc[:, my_cols].copy(deep=True)

# %%

# Load and prepare salinity data

## Create list of files
folder_in = "./data/interim/salinity"
glob_pattern = os.path.join(folder_in, "*.csv")
file_list = glob.glob(glob_pattern)

## Read files into a dataframe
salinity_data = pd.concat(
    (pd.read_csv(f) for f in file_list), 
    ignore_index=True
    )

## Change label and pivot table

dict_to_rename = {"Gatun Lake": "S_Gatun_Lake", 
                  "Miraflores Lake": "S_Miraflores",
                  "Atlantic Ocean": "S_Atlantic_Ocean",
                  "Pacific Ocean": "S_Pacific_Ocean",
                  "Agua Clara Upper Chamber": "N/A",
                  "Cocolí Upper Chamber": "N/A"}

salinity_data["Label"] = \
    salinity_data["Location"].replace(dict_to_rename)

salinity_data = salinity_data \
    .loc[salinity_data["Label"] != "N/A", :] \
    .pivot_table(index='Date', columns='Label', values='Value') \
    .dropna()

## Drop name of columns
salinity_data.columns.name = None

# %%

# Load and prepare transit data
file_name = "transits_daily.csv"
folder_in = "./data/interim/operations"
transit_data = pd.read_csv(os.path.join(folder_in, file_name))
transit_data.set_index("Date", inplace=True)

# %%

# Merge dataframes and create main dataframe

main_df = hydromet_data \
    .merge(salinity_data, how="inner",
           left_index=True, right_index=True) \
    .merge(transit_data, how="left",
           left_index=True, right_index=True) \
    .reset_index()

## Save main_df
folder_out = "./data/processed"
file_name = "mass_balance_data.ftr"
main_df.to_feather(os.path.join(folder_out, file_name))
# %%
