# Functions to join and process raw data
# M. G. Castrellon | 18 December 2023

# Load libraries
import os
import pandas as pd

# Define working directory
os.chdir("D:/SURFdrive/Projects/Conceptual_Model")

# Define function to concatenate an individual dataset from AQUARIUS
def join_aquarius_dataset(path_to_inputs, dataset_name):
    
    # Define path for the dataset to read
    path_to_dataset = os.path.join(path_to_inputs, dataset_name)

    # Create file list and empty dictionary to store data
    files = [f for f in os.listdir(path_to_dataset) if f.endswith('.csv')]
    data_dict = {file: [] for file in files}

    # Skip empty folders and send warning
    if len(files) == 0:
        print(f"Warning: Folder for {dataset_name} is empty.")
        return None

    # Read precipitation data
    if dataset_name == "precipitation":
        
        ## Read column names
        with open(path_to_dataset + "/" + files[0], 'r') as f:
            column_names = f.readlines()[1].split(',')
        
        ## Fix some column names
        column_names = [name.strip() for name in column_names]
        column_names[0] = "Start_Time"
        column_names[1] = "End_Time"

        ## Read files and populate dictionary
        for file in files:
            new_path = os.path.join(path_to_dataset, file)
            df = pd.read_csv(filepath_or_buffer=new_path, 
                             skiprows=5, header=None, 
                             names=column_names)
            
            ## Add df to dictionary
            data_dict[file] = df

        ## Create dataframe with all data
        dataset_df = pd.concat(data_dict.values(), ignore_index=True)

        ## Change dataframe to long format
        dataset_df = dataset_df \
            .drop(columns=['End_Time']) \
            .rename(columns={"Start_Time": "Date_Time"})
        dataset_df = pd.melt(
            dataset_df, id_vars=['Date_Time'], 
            var_name='Station', value_name='Value')

        ## Add new columns
        dataset_df['Unit'] = 'mm'
        dataset_df['Sensor'] = 'Precipitation.Telemetria'
    
    # Read other hydromet data
    else:

        ## Define column names
        if dataset_name == "air_tem" or dataset_name == "solar_radiation":
            column_names = ["Date_Time", "End_Time", "Value"]
        else:
            column_names = ["Date_Time", "Value"]

        ## Read files and populate dictionary
        for file in files:
            new_path = os.path.join(path_to_dataset, file)
            df = pd.read_csv(filepath_or_buffer=new_path, 
                             skiprows=2, header=None, 
                             names=column_names)
            
            ## Drop unnecessary columns
            df.drop(columns=['End_Time'], errors='ignore', inplace=True)

            ## Extract information from file
            with open(new_path, 'r') as f:
                lines = f.readlines()
            ### Extract first two lines
            line1 = lines[0].split(' - ')
            line2 = lines[1].split(',')
            ### Read sensor and station from first line
            sensor, station = line1[-1].split('@')
            station = station.split(',')[0]
            ### Read unit from second line
            unit = line2[-1].split(' ')[-1]
            unit = unit.strip(')\n').replace('(','')

            ## Add file info to df
            df['Unit'] = unit
            df['Sensor'] = sensor
            df['Station'] = station

            ## Add df to dictionary
            data_dict[file] = df
        
        ## Create dataframe with all data
        dataset_df = pd.concat(data_dict.values(), ignore_index=True)

    # Select columns of interest and return dataset
    etesa_cols = ['Station', 'Date_Time', 'Sensor', 'Value', 'Unit']
    dataset_df = dataset_df.loc[:, etesa_cols]
    dataset_df.drop_duplicates(inplace=True)
    return dataset_df
        
        
# Define function join data from AQUARIUS datasets
def join_aquarius_data(path_to_inputs, path_to_outputs):

    # Obtain dataset names
    dataset_names = [name for name in os.listdir(path_to_inputs) 
                 if os.path.isdir(os.path.join(path_to_inputs, name))]
    
    # Read, process and save
    for dataset_name in dataset_names:
        print(f"Processing {dataset_name} dataset...")
        out_fn = dataset_name + '.ftr'
        path_to_output_file = os.path.join(path_to_outputs, out_fn)
        df = join_aquarius_dataset(path_to_inputs, dataset_name)
        
        if df is None:
            print("Warning: Skipping dataset.")
            continue

        df = df.sort_values(by=['Station', 'Date_Time'])
        df.reset_index(drop=True, inplace=True)
        df.to_feather(path_to_output_file)
        print("Done with processing.")
    
# Join all data from individual stations
cwd = os.getcwd()
path_to_inputs = os.path.join(cwd, "./data/raw/aquarius")
path_to_outputs = os.path.join(cwd, "./data/interim/aq_joined")
join_aquarius_data(path_to_inputs, path_to_outputs)

"""
# Merge all hydromet data and save to feather
print("\nMerging all AQUARIUS data...")
all_files = [f for f in os.listdir(path_to_outputs) if f.endswith('.csv')]
data_dict = {file: [] for file in all_files}

for file in all_files:
    print(f"Processing {file}...")
    file_path = os.path.join(path_to_outputs, file)
    df = pd.read_csv(file_path, parse_dates=["Date_Time"])
    df["Date_Time"] = pd.to_datetime(df["Date_Time"])
    data_dict[file] = df

dataset_df = pd.concat(data_dict.values(), ignore_index=True)

fpath = os.path.join(path_to_outputs, "aquarius_data.ftr")
dataset_df.to_feather(fpath)

"""