# Script to process ACP salinity and water quality data
# M. G. Castrellon | 15 March 2024

# Laod libraries
import os
import glob
import pandas as pd

# Define working directory
os.chdir("D:/SURFdrive/Projects/Conceptual_Model")

# Define input and output folders
input_folder = os.path.join(os.getcwd(), "./data/raw/smithsonian/salinity")
output_folder = os.path.join(os.getcwd(), "./data/interim/salinity")

# Load water quality data
glob_pattern = os.path.join(input_folder, "./wqm_files/*.csv")
file_list = glob.glob(glob_pattern)

# Load data into a dataframe directory
wq_data = []
for file in file_list:
     fname = os.path.basename(file)[:-4]
     df = pd.read_csv(file)
     df["param"] = fname[4:]
     wq_data.append(df)

# Concatenate dataframes
wq_data = pd.concat(wq_data)

# Clean values
wq_data.rename(columns={"data": "value"}, inplace=True)
wq_data = wq_data.loc[wq_data["value"] >= 0, :].reset_index(drop=True)

# Load water quality locations
file_in = os.path.join(input_folder, "ACP WQM Stations (2021).xlsx")
wq_locations = pd.read_excel(file_in, sheet_name="Sheet1", usecols="A:G")

# Select columns of interest
my_cols = ["Code", "Name", "Water Body"]
wq_locations = wq_locations.loc[:, my_cols]

# Merge data and locations
df = pd.merge(wq_data, wq_locations, how="right",
              left_on="site", right_on="Code")

# Change column names and drop unnecessary columns
my_new_names = {"Name": "site_name", "Water Body": "water_body"}
df.rename(columns=my_new_names, inplace=True)
df.drop(columns=["Code"], inplace=True)

# Save dataframe
file_name = "salinity_wqm_all.feather"
df.to_feather(os.path.join(output_folder, file_name))
